# gym_beam
Welcome... to the pages. :ghost:

## Popis projektu

Jedná se o vypracování úkolu k výběrovému řízení na pozici: **Data Engineer pro GymBeam**. 

Poskytunuté zadání:

- zdani.md

## Použité nástroje
Při vypracování úkolu jsem použila nástroje:
- DB PostgreSQL
- Power BI
- draw.io


## Struktura repozitáře
```
gym_beam/
├── docs/
│   ├── img/
│   │   └── data_model.png
│   │   └── transform_data_model.png
│   ├── index.md
│   └── mkdocs_reseni.md
│   └── mkdocs_zadani.md
└── task/
│   ├── zadani.md
│   ├── reseni.md
├── .gitlab-ci-yml
├── Dockerfile
├── README.md
├── mkdocs.yml

```

## Pages
Pro přehlednost je použitá speciální funkce Pages, která umožňuje jednoduché publikování webových stránek přímo z GitLab repozitáře.

Zde je odkaz na [statické stránky](https://dialogy-tereza-juzova1-62117d03268e06233909e2284d127df51cdc1631.gitlab.io).

Je použita knihovna [MkDocs](https://www.mkdocs.org/) k tvorbě statické dokumentace webových stránek z formátovaného textu - makrdown.

V Pages se nachází jednoduchá struktura s popisem projektu, popis [zadání](https://dialogy-tereza-juzova1-62117d03268e06233909e2284d127df51cdc1631.gitlab.io/mkdocs_zadani/) a [řešení](https://dialogy-tereza-juzova1-62117d03268e06233909e2284d127df51cdc1631.gitlab.io/mkdocs_reseni/).

## Vypracování úkolu

V Pages v záložce řešení se nachází vypracování úkolu, jsou zde poskytnuty odpovědi na uvedené otázky. 

```
gym_beam/
└── task/
│   ├── zadani.md
│   ├── reseni.md
```

Testovací data se nachází v repozitáři zde:

```
gym_beam/
├── test_data/
│   ├── daily_stock.csv
│   ├── products.csv
│   ├── sales_order.csv
│   ├── sales_order_item.csv
```

## Přidání souborů

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/tereza.juzova1/dialogy.git
git branch -M main
git push -uf origin main
```