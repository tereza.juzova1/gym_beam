# Řešení

## Poskytnuté data sety:

![Input data sets](img/data_model.png)

## Datový model po transformaci
![Transform data model](img/transform_data_model.png)

## Transformace
Pro zjednodušení je vytvořen datový model pouze s daty/sloupci, které potřebuji pro zodpovězení otázek níže (nezajíma mě způsob dopravy, mezisoučet položek, měna atd.), i když z těchto informací by bylo možné vytvořit další zajímavé reporty.

Předpokládaná granularita je **den**. Data do datového skladu budu transformovat jednou za den, vždy za předchozí den. Důležité jsou produktu, které mají objednávky v konečném stavu, pro reporty je důležitý konečný **stav objednávky = complete**. Nezapočítávám do faktové tabulky produkty, které mají objednávku ve stavu pending, nebo canceled. Protože náš zajímá finální stav objednávky, je pro mě důležité datum: **UPDATED_AT** (Předpokládám, že nejdříve je objednávka ve stavu pending a poté se až dostane do finálního stavu). Každá tabulka bude mít systémové sloupce označené prefixem **S_**, vždy sloupec **S_LOAD_ID** - označení loadu, ve kterém byl záznam vytvořen a **S_LOAD_TIME**, časové razítko, kdy byl záznam vytvořen. Předpokládá se, že bude existovat také logovací tabulka, která bude udžovat přehled jednotlivých loadů, dobu trvání každého loadu (zpracování) a jestli skončila úspěšně. V modelu logovací tabulky nejsou zakresleny.

**Zjednodušená transformace pomocí SQL:**

```
CREATE TABLE D_PRODUCT AS
SELECT
    PRODUCT_ID,
    PRODUCT_NAME
FROM
    PRODUCTS;
```
```
CREATE TABLE D_DATE (
    DATE_ID INT PRIMARY KEY,
    DAY INT,
    MONTH INT,
    YEAR INT   
);

INSERT INTO D_DATE (DATE_ID, DAY, MONTH, YEAR)
SELECT
    CAST(DATE AS INT) AS DATE_ID,
    DAY(DATE),
    MONTH(DATE),
    YEAR(DATE)
FROM
    DAILY_STOCK;
```

```
CREATE TABLE F_SALES AS
SELECT
    ROW_NUMBER() OVER (ORDER BY DATE) AS SALES_ID,
    d.DATE_ID,
    d.PRODUCT_ID,
    d.SOLD_QTY,
    d.ON_HAND_QTY,
    IS_BESTSELLER, -- U_LAST_MONTH_SALES
    SOLD_OUT_DAYS, -- U_SOLD_OUT_DAYS
    AVG_SALES_LAST_90_DAYS -- U_SALES_LAST_90_DAYS
FROM (
    SELECT
        DATE(UPDATED_AT) AS DATE_ID,
        PRODUCT_ID,
        SUM(ORDERED_QTY) AS SOLD_QTY,
        ON_HAND_QTY
    FROM
        SALES_ORDER_ITEM
    GROUP BY
        UPDATED_AT,
        PRODUCT_ID,
        ON_HAND_QTY
) d
LEFT JOIN (
    SELECT
        DATE(UPDATED_AT) AS DATE_ID,
        PRODUCT_ID,
        ORDER_ID
    FROM
        SALES_ORDER
    WHERE
        STATUS = 'complete'
) s ON d.DATE_ID = s.DATE_ID AND d.PRODUCT_ID = s.PRODUCT_ID;

```
Sloupce z faktové tabulky, označeny **modře**, mají za úkol usnadnit vytváření reportů a dashboardů v BI nástroji. V transformaci jsme schopni je doplnit díky pomocným tabulkám (také označeny modře) s prefixem **U_**, což znamená **utility**. 

- Tabulka - **U_LAST_MONTH_SALES** - se napočívá jednou za měsíc a jsme schopni zní zjistit, zda daný produkt je bestseller, či nikoliv. Udržujeme pouze data za poslední měsíc.
- Tabulka - **U_SOLD_OUT_DAYS** - se napočítává každý den a díky ní jsme schopni zjistit, kolik dní už je produkt vyprodaný, každý den se změní stav - počet dní. Udžujeme pouze informace o vyprodaných produktech.
- Tabulka - **U_SALES_LAST_90_DAYS** - se napočítává každý den a jsme z ní schopni zjistit průměrné prodeje produktů za posledních 90 dní. 


### Výsledek:
Dimenzionální tabulka - **D_PRODUCT**

- **PRODUCT_ID** - jednoznačný identifikátor produktu
- **PRODUCT_NAME** - jméno produktu
- **S_LOAD_ID** - identifikátor loadu, ve kterém se záznam vytvořil
- **S_LOAD_TIME** - časové razítko, kdy byl záznam vytvořen

Dimenzionální tabulka- **F_SALES**

- **DATE_ID** - jednoznačný identifikátor datumu, ve formátu YYYYMMDD
- **DAY**
- **MONTH**
- **YEAR**
- **DATE** - v datumovém formátu
- a možné další sloupce, které obohacují dimenzionální tabulku
- **S_LOAD_ID** - identifikátor loadu, ve kterém se záznam vytvořil
- **S_LOAD_TIME** - časové razítko, kdy byl záznam vytvořen

 Faktová tabulka - **F_SALES**

- **SALES_ID** - jednoznačný identifikátor
- **DATE_ID** - vždy datum za předešlý den, je to FK na dimenzionální tabulku D_DATE
- **PRODUCT_ID** - FK na dimenzionální tabulku D_PRODUCT
- **SOLD_QTY** - počet prodaných produktů
- **ON_HAND_QTY** - stav zásob na konci dne
- **IS_BESTSELLER** - TRUE/FALSE, jestli se jedná o bestseller
- **SOLD_OUT_DAYS** - počet dní za sebou, kdy je produkt vyprodaný
- **AVG_SALES_LAST_90_DAYS** - průměrný prodej za posledních 90 dní
- **S_LOAD_ID** - identifikátor loadu, ve kterém se záznam vytvořil
- **S_LOAD_TIME** - časové razítko, kdy byl záznam vytvořen

## Otázky

Tak jak máme připravený datový model, tak jsme z něho schopni odpovědět na všechny níže uvedené otázky. Není potřeba vytvářet žádné další speciální datasety/datamarty pro konkrétní reporty. Pro přehlednost také přidávám sql dotazy, které by mi daný report vytvořily. Aby bylo vidět, co očekávám za výsledek. Stejný výsledek, bych poté očekávala v BI nástroji, například PowerBI. V PowerBI jsem schopná poskládat reporty celkem jednoduše, bez příliš složitých selectů a krkolomných úprav.

```
Pozn.: 
    - Selecty byly testované v PostgreSQL
    - uvažuji o BI nástroji - PowerBI
```

### Jaká byla prodejnost produktů v minulosti

Na tento case potřebujeme data:
 
- **D_DATE**
- **D_PRODUCT**
- **F_SALES**

```
SELECT
	s.PRODUCT_ID,
	p.PRODUCT_NAME,
	s.DATE_ID,
	SUM(s.ORDERED_QTY) AS TOTAL_SOLD_QTY
FROM
    F_SALES s
JOIN
	D_PRODUCT p
ON
	p.PRODUCT_ID = s.PRODUCT_ID    
GROUP BY
    s.PRODUCT_ID,
	p.PRODUCT_NAME,
	s.DATE_ID	
ORDER BY
    s.DATE_ID DESC;
```

**Jen pro porovnání, jak bych tento report poskládala z původních datasetů:**

Na tento case potřebujeme data:

- tabulka: **SALES_ORDER_ITEM**
    - sloupce: 
        - SALES_ORDER_ITEM.ORDERED_QTY
        - SALES_ORDER_ITEM.PRODUCT_ID
        - SALES_ORDER_ITEM.UPDATED_AT


- tabulka: **PRODUCT**
    - PRODUCT.PRODUCT_NAME -  pokud budeme chtít názvy produktů

- tabulka: **SALES_ORDER**
    - sloupec: SALES_ORDER.STATUS, předpokládám, že nás zajímají pouze uskutečněné prodeje, tedy status = 'complete'

Můžeme si připravit přímo dataset, který by mohl vypadat takto:

```
SELECT
	s.PRODUCT_ID,
	p.PRODUCT_NAME,
	DATE(s.UPDATED_AT) AS DATE,
	SUM(s.ORDERED_QTY) AS TOTAL_SOLD_QTY
FROM
    SALES_ORDER_ITEM s
JOIN
	PRODUCT p
ON
	p.PRODUCT_ID = s.PRODUCT_ID
JOIN 
	SALES_ORDER so
ON	
	so.ORDER_ID = s.ORDER_ID
WHERE so.STATUS = 'complete'    
GROUP BY
    s.PRODUCT_ID,
	p.PRODUCT_NAME,
	s.UPDATED_AT	
ORDER BY
    s.UPDATED_AT DESC;
```

Pro tento case je také možné použít data přímo ve stavu jakém jsou a v **BI nástroji** připravit report. Kde bychom použili filtr na datum (granualarita až na konkrétní dny) a status (zaškrtnuto pouze pro complete), případně nás mohou zajímat i jiné statusy, filtr bych také použila na produkty, abychom se mohli podívat na vývoj jednotlivých produktů v čase.

### Kolik produktů bylo v minulosti na konci dne vyprodaných a které produkty to byly?

Na tento case potřebujeme data:
 
- **D_DATE**
- **D_PRODUCT**
- **F_SALES**

```
SELECT
    distinct
	s.PRODUCT_ID,
	p.PRODUCT_NAME
FROM
    F_SALES s
JOIN
	D_PRODUCT p
ON
	p.PRODUCT_ID = s.PRODUCT_ID    
WHERE   
    ON_HAND_QTY = 0;
```

### Jaké produkty byly vyprodané 5 a více dní po sobě?

Na tento case potřebujeme data:
 
- **D_DATE**
- **D_PRODUCT**
- **F_SALES**

```
SELECT
    s.PRODUCT_ID,
    p.PRODUCT_NAME
FROM
    F_SALES s
JOIN
     D_PRODUCT  p
ON    
    s.PRODUCT_ID = p.PRODUCT_ID
WHERE
    s.SOLD_OUT_DAYS >= 5;
```

### Které produkty byly vyprodané nejdéle?

Na tento case potřebujeme data:
 
- **D_DATE**
- **D_PRODUCT**
- **F_SALES**

```
SELECT
    s.PRODUCT_ID,
    p.PRODUCT_NAME,
    MAX(s.SOLD_OUT_DAYS) AS MAX_SOLD_OUT_DAYS
FROM
    F_SALES s
JOIN
     D_PRODUCT  p
ON    
    s.PRODUCT_ID = p.PRODUCT_ID
GROUP BY
    s.PRODUCT_ID,
    p.PRODUCT_NAME;
```

### Kolik bestsellerů bylo v minulosti na konci dne vyprodanych? Které to byly? 

Na tento case potřebujeme data:
 
- **D_DATE**
- **D_PRODUCT**
- **F_SALES**

```
SELECT
    distinct
    s.PRODUCT_ID,
    p.PRODUCT_NAME   
FROM
    F_SALES s
JOIN
     D_PRODUCT  p
ON    
    s.PRODUCT_ID = p.PRODUCT_ID
WHERE
      ON_HANDS_QTY = 0 AND  IS_BESTSELLER = TRUE;
```

### Kdy očekáváme vyprodání zásob jednotlivých produktů, pokud budeme vycházet z aktualní skladové zásoby a průměrnému prodeji za posledních 90 dní?

Na tento case potřebujeme data:
 
- **D_DATE**
- **D_PRODUCT**
- **F_SALES**

```
SELECT
    s.PRODUCT_ID,
    p.PRODUCT_NAME,
    s.ON_HAND_QTY/s.AVG_SALES_LAST_90_DAYS AS DAYS_TO_SOLL_OUT
FROM 
    F_SALES s
JOIN
    D_PRODUCT p
ON
    s.PRODUCT_ID = p.PRODUCT_ID
JOIN
    D_DATE d
ON    
    CURRENT_DATE = d.DATE;    
```

**Pozn. k PowerBI**

V Power BI existují různé způsoby, jak provádět analýzu predikce budoucích hodnot na základě historických dat. Můžeme využít nástroje pro predikci, jakou jsou prediktivní modely a funkce pro časové řady. 

- Power BI má vestavěnou funkci pro analýzu časových řad, jako je Exponential Smoothing nebo ARIMA (Autoregressive Integrated Moving Average)

- DAX Funkce (Data Analysis Expressions) - v Power BI můžeme vytvářet vlastní DAX pro predikci a analýzu dat

- Power BI umož%nuje integraci skriptů v jazycích R a Python pro pokročilou analýzu dat a predikci dat

## Shrnutí

Otázka je, kdy k tomu přistoupit jako k tvorbě samostatného datamartu, nebo kdy využít možností, které nám nabízejí samotné BI nástroje. Také záleží na tom, jestli člověk, který vytváří reporty v BI nástorji je technický a dokáže si poradit se složitejšími dotazy. Pokud by si reporty připravoval business uživatel, volilala bych datasety vždy tak, aby pro něho práce v BI nástrojích byla, co nejjednodušší. 

Datamart, se hodí pokud potřebujeme komplexní analýzy s optimalizovanými dotazy, zahrnující různé zdroje a specializované na konkrétní oblast.

BI nástroj použijeme pokud potřebujeme rychlé a interaktivní vizualizace, reporty nebo dashboardy, kde je důraz na snadnou tvorbu a přizpůsobení.

V některých případech mohou být tyto přístupy kombinovány. Datamart může sloužit jako základní úložiště dat, ze kterého se následně v BI nástroji vytvářejí vizualizace a analýzy. Každý případ je závislý na konkrétních potřebách a složitosti datových požadavků.


**Úpravy dat přímo v BI nástrojích:**

**Rychlost a flexibilita:** Upravy dat v BI nástrojích mohou být rychlejší a flexibilnější, pokud se jedná o jednorázové nebo ad-hoc úpravy dat pro konkrétní analýzu.

**Nároky na údržbu:** Úpravy dat přímo v BI nástrojích mohou být snazší na správu a údržbu, protože nemusíte udržovat oddělenou datovou strukturu (datamarty).

**Aktualita dat:** Upravy provedené přímo v BI nástrojích mohou rychle zohlednit aktuální stav dat bez nutnosti aktualizovat celý datamart.

**Vytvoření datamartu:**

**Optimalizace výkonu:** Vytvoření datamartu umožňuje optimalizovat výkon analýz tím, že předem zpracovaná a agregovaná data jsou k dispozici. To může zvýšit rychlost odpovědí na dotazy.

**Komplexní analýzy:** Datamarty umožňují provádět složitější analýzy, které mohou vyžadovat spojení a zpracování dat z různých zdrojů. To může být efektivní pro konzistentní analýzu většího množství otázek.

**Oddělení datových zdrojů:** Vytvoření datamartu může pomoci oddělit analytická data od operačních datových zdrojů, což může zvýšit stabilitu a bezpečnost.

Celkově je vhodné zvážit potřeby uživatelů, charakter analýz, nároky na výkon, dostupnost a komplexnost dat, rychlost aktualizací a další faktory. Někdy může být vhodné kombinovat oba přístupy - vytvořit datamart pro složitější analýzy a zároveň umožnit úpravy dat přímo v BI nástrojích pro ad-hoc analýzy. Každá situace je specifická, a proto je důležité provést důkladný průzkum a zhodnocení možností předtím, než se rozhodnete pro konkrétní přístup.