# Zadání
## Case Study - Data Engineer

**Predstav si, že ako dátový inžinier/dátová inžinierka dostaneš za úlohu pripraviť pre
kolegov z oddelenia nákupu reporty a dashboardy, ktoré im majú pomôcť
efektívnejšie objednávať tovar.**

Hlavné otázky, ktoré potrebujeme zodpovedať sú:

- Kedy očakávame vypredanie zásob jednotlivých produktov, ak vychádzame z
aktuálnej skladovej zásoby a priemernej predajnosti za posledných 90 dní?
- Aká bola predajnosť produktov v minulosti
- Koľko produktov bolo v minulosti na konci dňa vypredaných? Ktoré to boli?
- Aké produkty boli vypredané 5 a viac dní po sebe?
- Ktoré produkty boli vypredané najdlhšie?
- Koľko bestsellerov bolo v minulosti na konci dňa vypredaných? Ktoré to boli?
Bestseller identifikujeme ako produkt, ktorý patrí medzi top 10% produktov podľa
predaných kusov v predchádzajúcom kalendárnom mesiaci. Každý kalendárny
mesiac môžu byť bestsellermi iné produkty.

Tvojou úlohou je navrhnúť, aké datasety je potrebné pripraviť pre vizualizáciu odpovedí na
tieto otázky. Budeme pri tom predpokladať, že výstupy budú v BI nástroji podľa tvojho
výberu, prípadne spracované v Exceli pomocou pivot tabuliek. Počítaj s tým, že užívatelia
budú chcieť filtrovať konkrétne produkty, časové obdobia a tiež vidieť vývoj metrík v čase.

Pre vypracovanie case study nemáš k dispozícií potrebné vstupné dáta, len popis tabuliek, z
ktorých je možné výstup vyskladať. Výstup, ktorý od teba očakávame:
- zadefinovanie datasetu, alebo viacerých datasetov, ktoré by si načítal v BI nástroji
alebo exceli. V prípade BI nástroja napíš aj s akým konkrétnym uvažuješ, a v prípade
viacerých datasetov popíš aj dátový model, ktorý by spolu tvorili (jednotlivé
prepojenia dátového modelu)
- jednoduchý popis transformácií, ktoré by si musel spraviť pre dosiahnutie finálnych
datasetov.

Dáta, ktoré máte k dispozícií:

Tabuľka **DAILY_STOCK** - obsahuje stav zásob jednotlivých produktov ku koncu dňa.

- ID - jedinečné ID
- DATE - dátum stavu zásob
- PRODUCT_ID - ID produktu
- ON_HAND_QTY - stav zásob ku koncu dňa


Tabuľka **PRODUCTS** - katalóg produktov

- PRODUCT_ID - ID produktu
- CREATED_AT - kedy bol produkt vytvorený v databáze
- PRODUCT_NAME - názov produktu
- IS_LONG_TERM_UNAVAIBLE - boolean, TRUE ak nie je možné objednať u
dodávateľa

Tabuľka **SALES_ORDER** - hlavička objednávok zákazníkov (predané produkty)
- ORDER_ID - ID objednávky
- STATUS - stav objednávky
    - complete, sent - objednávka je odoslaná
    - canceled - objednávka bola zrušená, tovar nebol expedovaný
    - pending, processing - objednávka sa spracováva, tovar ešte neodišiel zo
skladu
- CUSTOMER_ID - jedinečné ID zákazníka
- BASE_SUBTOTAL - medzisúčet položiek, bez dopravy, dane a zliav (v mene
zákazníka)
- BASE_CURRENCY_CODE - skratka základnej meny
- BASE_TO_GLOBAL_RATE - výmenný kurz zo základnej do globálnej meny (EUR)
- GLOBAL_CURRENCY_CODE - skratka globálnej meny
- SHIPPING_METHOD - kód spôsobu dopravy
- CREATED_AT - timestamp vytvorenia objednávky
- UPDATED_AT - timestamp aktualizácie objednávky

Tabuľka **SALES_ORDER_ITEM** - položky na objednávkach

- ORDER_ITEM_ID - unikátne ID riadku v tabuľke
- ORDER_ID - ID objednávky
- CREATED_AT - timestamp vytvorenia položky na danej objednávke
- UPDATED_AT - timestamp aktualizácie položky na danej objednávke
- PRODUCT_ID - ID produktu
- ORDERED_QTY - počet objednaných kusov produktu v objednávke
- BASE_PRICE - základná cena produktu
- BASE_ROW_TOTAL - základná cena produktu vynásobená počtom objednaných
kusov
- IS_FREE_SHIPPING - boolean, TRUE ak je doručenie zdarma